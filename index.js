/**
 * Created by rickytsang on 23/3/2017.
 */

/* import the 'restify' module and create an instance. */
const restify = require('restify')
const server = restify.createServer()

//heroku: https://weatherreporthistory.herokuapp.com
//gitlab: https://gitlab.com/rickytg/305cde_assignment2

/* import the required plugins to parse the body and auth header. */
//server.use(restify.fullResponse())
//server.use(restify.bodyParser())
//server.use(restify.authorizationParser())


/* import our custom module. */
const weather = require('./weather.js');
var mongoose = require('mongoose');
var url = 'mongodb://testing:testing@ds135690.mlab.com:35690/rickytg';

mongoose.connect(url);

//connect to mongoDB
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
    console.log("Database Connected.");

});

db.on('error', function (err) {
    console.log('connection error', err);
});
db.once('open', function () {
    console.log('connected.');
});

//APIs
server.get('/api/weather/:id', getWeatherByCountry);
server.get('/api/searchHistory/:id', getWeatherReportByDate);

//get weather api
function getWeatherByCountry(req, res, next){
    console.log('get weather api called');
    var tempCountry = ""

    if (!req.params.id){
        console.log('country is empty, set to default country: Hong Kong');
        tempCountry = "Hong Kong"
    }else{
        tempCountry = req.params.id;
    }
    weather.search(tempCountry, function(result) {
        console.log(result)
        insertWeatherReport(result);

        /* we need to set the content-type to match the data we are sending. We then send the response code and body. Finally we signal the end of the response. */
        res.contentType = 'json';

        res.send(200, result);
        res.end();
    })
}

//get search weather history report api
function getWeatherReportByDate(req, res, next) {
    console.log('get weather report api called');
    var tempDate = ""

    if (!req.params.id){
        console.log('the date is empty, set to default day is Today');
        var splitDate = new Date().toString().split(' ')
        tempDate = splitDate[1] + ' ' + splitDate[2] + ' ' + splitDate[3]
    }else{
        tempDate = req.params.id;
    }

    WeatherReport.find({date_time:tempDate}, (function (err, result) {// /^
        if (err) return console.error(err);
        console.log(result);

        res.contentType = 'json';

        res.send(200, result);
        res.end();
    }))
}

var Schema = mongoose.Schema;

// create weather schema
var weatherReportSchema = new Schema({
    city_code : String,
    city_name: String,
    weather_main: String,
    weather_desc: String,
    temp: String,
    pressure: String,
    humidity: String,
    temp_min: String,
    temp_max: String,
    wind_speed: String,
    date_time: String
});

// create model using the schema
var WeatherReport = mongoose.model('WeatherReport', weatherReportSchema);

function insertWeatherReport(result) {
    /*
        city_code: result[],
        city_name: data.name,
        weather_main: data.weather[0].main,
        weather_desc: data.weather[0].description,
        temp: data.main.temp,
        pressure: data.main.pressure,
        humidity: data.main.humidity,
        temp_min: data.main.temp_min,
        temp_max: data.main.temp_max,
        wind_speed:data.wind.speed
    });
    */

    // data from the returned json
    var weatherReportObject = new WeatherReport({
        city_code: result['city_code'],
        city_name: result['city_name'],
        weather_main: result['weather_main'],
        weather_desc: result['weather_desc'],
        temp: result['temp'],
        pressure: result['pressure'],
        humidity: result['humidity'],
        temp_min: result['temp_min'],
        temp_max: result['temp_max'],
        wind_speed: result['wind_speed'],
        date_time: result['date_time']
    });

    // pass the data to the mongoDB
    weatherReportObject.save(function (err, data) {
        if (err) console.log(err);
        else console.log('Saved ', data );
    });

    console.log('Inserted weather report : ',WeatherReport);

}

/*
// this route provides a URL for the 'lists' collection. It demonstrates how a single resource/collection can have multiple representations.
server.get('/', function(req, res, next) {
    var country = "London"
    weather.search(country, function(result) {
        console.log(result)

        // we need to set the content-type to match the data we are sending. We then send the response code and body. Finally we signal the end of the response.
        res.contentType = 'json';
        res.send(200, result);
        res.end();
    })
})
*/

/* This route points to the 'lists' collection. The POST method indicates that we indend to add a new resource to the collection. Any resource added to a collection using POST should be assigned a unique id by the server. This id should be returned in the response body. */
/*server.post('/lists', function(req, res) {
    console.log('adding a new list')
    // The req object contains all the data associated with the request received from the client. The 'body' property contains the request body as a string.
    const body = req.body
    /* Since we are using the authorization parser plugin we gain an additional object which contains the information from the 'Authorization' header extracted into useful information. Here we are displaying it in the console so you can understand its structure.
    const auth = req.authorization
    console.log(auth)
    const data = lists.addNew(auth, body)
    res.setHeader('content-type', data.contentType)
    res.send(data.code, data.response)
    res.end()
})*/

/* The PUT method is used to 'update' a named resource. This is not only used to update a named resource that already exists but is also used to create a NEW RESOURCE at the named URL. It's important that you understand how this differs from a POST request. */
/*server.put('/lists/:listID', function(req, res) {
    res.setHeader('content-type', 'application/json')
    res.send(data.code, {status: data.status, message: 'this should update the specified resource'})
    res.end()
})*/

/* The DELETE method removes the resource at the specified URL. */
/*server.del('/lists/:listID', function(req, res) {
    res.setHeader('content-type', 'application/json')
    res.send(data.code, {status: data.status, message: 'this should delete the specified resource'})
    res.end()
})*/

const port = process.env.PORT || 8088
server.listen(port, function(err) {
    if (err) {
        console.error(err)
    } else {
        console.log('App is ready at : ' + port)
    }
})