/**
 * Created by rickytsang on 11/4/2017.
 */
var frisby = require('frisby');
frisby.create('get london weather')
    .get('http://localhost:8088/api/weather/london')
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .toss()

frisby.create('get weather by did not type anything')
    .get('http://localhost:8088/api/weather/')
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .toss()

frisby.create('get hong kong weather')
    .get('http://localhost:8088/api/weather/hong kong')
    .expectHeaderContains('content-type', 'application/json')
    .expectStatus(200)
    .toss()

frisby.create('get 04-10-2017 weather search history')
    .get('http://localhost:8088/api/searchHistory/Apr 10 2017')
    .expectHeaderContains('content-type', 'application/json')
    .expectStatus(200)
    .toss()

frisby.create('get weather search history by did not type anything')
    .get('http://localhost:8088/api/searchHistory/Apr 10 2017')
    .expectHeaderContains('content-type', 'application/json')
    .expectStatus(200)
    .toss()