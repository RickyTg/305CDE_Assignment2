/**
 * Created by rickytsang on 23/3/2017.
 */
const url = "http://api.openweathermap.org/data/2.5/weather";
const api_id = "7ce4ebf889f29ebb23bfa4f32553adbb";

const request = require('request');
exports.search = function(city, callback){
    var params = {appid:api_id, q:city, units:'metric'}
    request ({url:url, qs:params, method:'GET'}, function(err, res, content){
        if(err){
            console.log('ERROR: '+err.message)
        }else{
            var data = JSON.parse(content)
            var splitDate = new Date().toString().split(' ')
            callback({
                city_code: data.sys.country,
                city_name: data.name,
                weather_main: data.weather[0].main,
                weather_desc: data.weather[0].description,
                temp: data.main.temp,
                pressure: data.main.pressure,
                humidity: data.main.humidity,
                temp_min: data.main.temp_min,
                temp_max: data.main.temp_max,
                wind_speed: data.wind.speed,
                date_time: splitDate[1] + ' ' + splitDate[2] + ' ' + splitDate[3]
            })
        }
    })
};